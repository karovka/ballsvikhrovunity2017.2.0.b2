﻿
namespace Assets.Scripts.UI.Controllers
{
    public interface IBaseViewController
    {
        void Show();
        void Hide();
        void Init();
    }
}
