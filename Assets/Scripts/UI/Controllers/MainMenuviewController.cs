﻿using Assets.Scripts.GameCore;
using Assets.Scripts.UI.VIews;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.UI.Controllers
{
    public class MainMenuviewController:BaseViewController<IMainMenuView,IGameRunner>
    {
        public override void Init()
        {
            IView.StartButtonClickedEvent.AddListener(()=>
            {
                IDataModel.StartGame();
            });
        }

        public MainMenuviewController(DiContainer container,Canvas canvas) : base(container,canvas)
        {
        }
    }
}
