﻿using Assets.Scripts.GameCore;
using Assets.Scripts.UI.VIews;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.UI.Controllers
{
    public class HudMenuViewController:BaseViewController<IHudView,IGameSession>
    {
        public HudMenuViewController(DiContainer container, Canvas canvas) : base(container,canvas)
        {
        }

        public override void Show()
        {
            base.Show();
            Init();
            ReDraw();
            IDataModel.OnChange += ReDraw;
        }

        public override void Init()
        {
            IView.CurrentScore = 0;
            IView.MissedScore = 0;
            IView.ReaminingSeconds = 0;
        }

        public override void Hide()
        {
            base.Hide();
            IDataModel.OnChange -= ReDraw;
        }

        private void ReDraw()
        {
            IView.CurrentScore = IDataModel.CurrentScore;
            IView.MissedScore = IDataModel.MissedScore;
            IView.ReaminingSeconds = IDataModel.RemaininTime;
        }
    }
}
