﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.UI.Controllers
{
    public abstract class BaseViewController<TV,TDm>:IBaseViewController where TV: IView where TDm: class
    {
        protected TV IView;

        protected TDm IDataModel;


        protected BaseViewController(DiContainer container, Canvas canvas)
        {
            IView = container.Resolve<TV>();
            IView.SetParent(canvas.transform);
            IView.Hide();
            IDataModel = container.Resolve<TDm>();
        }
        public virtual void Hide()
        {
            IView.Hide();
        }

        public virtual void Show()
        {
            IView.Show();
            IView.TopThisView();
        }

        public abstract void Init();
    }
}
