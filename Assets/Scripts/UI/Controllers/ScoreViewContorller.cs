﻿using Assets.Scripts.GameCore;
using Assets.Scripts.UI.VIews;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.UI.Controllers
{
    public class ScoreViewContorller:BaseViewController<IScoreView,IGameRunner>
    {
        public ScoreViewContorller(DiContainer container,Canvas canvas) : base(container,canvas)
        {
        }

        public override void Init()
        {
            IView.GoToMainMenuButtonEvent.AddListener(IDataModel.ShowStartMenu);
            IView.RestartButtonEvent.AddListener(IDataModel.StartGame);
        }

        public override void Show()
        {
            base.Show();
            Draw();
        }

        private void Draw()
        {
            IView.CurrentScore = IDataModel.LastScore();
        }
    }
}
