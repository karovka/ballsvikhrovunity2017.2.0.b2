﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.UI.Controllers;

namespace Assets.Scripts.UI.VIews
{
    public interface IHudView:IView
    {
        int CurrentScore { set ; }
        int MissedScore { set ; }
        int ReaminingSeconds { set; }
    }
}
