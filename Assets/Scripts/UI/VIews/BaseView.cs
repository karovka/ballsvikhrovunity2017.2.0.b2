﻿using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.UI.Controllers
{
    public abstract class BaseView : MonoBehaviour, IView
    {
        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        public void SetParent(Transform tr)
        {
            gameObject.transform.SetParent(tr,false);
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public void TopThisView()
        {
            transform.SetAsLastSibling();
        }
    }
}
