﻿using Assets.Scripts.UI.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.VIews
{
    class HudViewMediator:BaseView,IHudView
    {
        [SerializeField] public Text _currentScore;
        [SerializeField] public Text _currentMissed;
        [SerializeField] public Text _remainningSeconds;

        public int CurrentScore
        {
            set { _currentScore.text = "Score: "+value.ToString(); }
        }

        public int MissedScore
        {
            set { _currentMissed.text = "Missed Score: " + value.ToString(); }
        }

        public int ReaminingSeconds
        {
            set { _remainningSeconds.text = "Reamining Time: " + value.ToString(); }
        }
    }
}
