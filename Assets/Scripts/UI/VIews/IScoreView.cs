﻿using Assets.Scripts.UI.Controllers;
using UnityEngine.UI;

namespace Assets.Scripts.UI.VIews
{
    public interface IScoreView:IView
    {
        int CurrentScore { set; }
        Button.ButtonClickedEvent GoToMainMenuButtonEvent { get; }
        Button.ButtonClickedEvent RestartButtonEvent { get; }
    }
}
