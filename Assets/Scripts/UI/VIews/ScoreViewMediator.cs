﻿
using Assets.Scripts.UI.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.VIews
{
    class ScoreViewMediator:BaseView,IScoreView
    {
        [SerializeField] private Button _goToMainMenuButton;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Text _scoreText;

        public int CurrentScore
        {
            set{ _scoreText.text = "Your Score: " + value; }
        }

        public Button.ButtonClickedEvent GoToMainMenuButtonEvent
        {
            get { return _goToMainMenuButton.onClick; }
        }

        public Button.ButtonClickedEvent RestartButtonEvent
        {
            get { return _restartButton.onClick; }
        }
    }
}
