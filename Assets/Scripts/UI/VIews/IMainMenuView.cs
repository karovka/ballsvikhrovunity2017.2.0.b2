﻿using Assets.Scripts.UI.Controllers;
using UnityEngine.UI;

namespace Assets.Scripts.UI.VIews
{
    public interface IMainMenuView:IView
    {
        Button.ButtonClickedEvent StartButtonClickedEvent { get; }
    }
}
