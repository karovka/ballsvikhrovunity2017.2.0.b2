﻿using System;
using UnityEngine;

namespace Assets.Scripts.UI.Controllers
{
    public interface IView
    {
        void SetParent(Transform tr);
        void Show();
        void Hide();
        void TopThisView();
    }
}
