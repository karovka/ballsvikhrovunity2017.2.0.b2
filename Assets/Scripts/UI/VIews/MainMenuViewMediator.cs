﻿using Assets.Scripts.UI.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.VIews
{
    public class MainMenuViewMediator:BaseView,IMainMenuView
    {
        [SerializeField] private Button _startButton;

        public Button.ButtonClickedEvent StartButtonClickedEvent
        {
            get { return _startButton.onClick; }
        }
    }
}
