﻿using Assets.Scripts.UI.Controllers;

namespace Assets.Scripts.UI
{
    public interface IUiManager
    {
        T GetOrCreate<T>() where T : class, IBaseViewController;
        void HideAllViews();
    }
}
