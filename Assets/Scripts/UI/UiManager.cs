﻿using Assets.Scripts.UI.Controllers;
using System;
using System.Collections.Generic;
using Assets.Scripts.UI.VIews;
using UnityEngine;
using Zenject;
using Assets.Scripts.UI;
using System.Linq;
using UnityEngine.UI;

public class UiManager: IInitializable,IUiManager
{
    [Inject] private DiContainer _container;
    [Inject] public Canvas _canvas { get; private set; }

    private Dictionary<Type, IBaseViewController> _views = new Dictionary<Type, IBaseViewController>();

    public T GetOrCreate<T>() where T: class, IBaseViewController
    {
        if (!_views.ContainsKey(typeof(T)))
        {
            var t = Activator.CreateInstance(typeof(T), new object[] { _container, _canvas}) as T;
            t.Init();
            _views.Add(typeof(T), t);
        }
        return _views[typeof(T)] as T;
    }

    public void Initialize()
    {
        _container.Bind<IMainMenuView>().FromComponentInNewPrefabResource("UI/MainMenuView").AsTransient();
        _container.Bind<IHudView>().FromComponentInNewPrefabResource("UI/HudView").AsTransient();
        _container.Bind<IScoreView>().FromComponentInNewPrefabResource("UI/ShowScoreView").AsTransient();
    }

    public void HideAllViews()
    {
        foreach (var view in _views)
        {
            view.Value.Hide();
        }
    }
}
