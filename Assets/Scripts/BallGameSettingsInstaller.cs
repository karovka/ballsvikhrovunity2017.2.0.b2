﻿
using System;
using Assets.Scripts.GameCore;
using UnityEngine;
using Zenject;

namespace Assets.Scripts
{

    [CreateAssetMenu(fileName = "Settings", menuName = "Settings/CreateBallsGameSetting", order = 1)]
    public class BallGameSettingsInstaller : ScriptableObjectInstaller<BallGameSettingsInstaller>
    {
        public GameSession.Settings GameSessionSettings;
        public BallGenerator.Settings BallGeneratorSettings;
        public SpriteBall.SpriteBallSetting SpriteBallSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(GameSessionSettings);
            Container.BindInstance(BallGeneratorSettings);
            Container.BindInstance(SpriteBallSettings);
        }
    }
}
