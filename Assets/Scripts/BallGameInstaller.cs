﻿using Assets.Scripts.GameCore;
using Zenject;

namespace Assets.Scripts
{
    public class BallGameInstaller: MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<UiManager>().AsSingle();
            Container.BindInterfacesTo<GameRunner>().AsSingle();
            Container.BindInterfacesTo<GameSession>().AsSingle();
            Container.DeclareSignal<BallSignal>();
            Container.DeclareSignal<GameStatusChangeSignal>();
        }

    }
}
