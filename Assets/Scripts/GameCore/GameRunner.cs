﻿using Assets.Scripts.UI;
using Assets.Scripts.UI.Controllers;
using Zenject;

namespace Assets.Scripts.GameCore
{
    public enum GameChangeType
    {
        Started,
        Ended,
        ExitToMainMenu
    }

    public class GameStatusChangeSignal : Signal<GameStatusChangeSignal, GameChangeType>
    {
    }
    public class GameRunner: IInitializable, IGameRunner
    {
        [Inject] private IGameSession _gameSession;
        [Inject] private IUiManager _uiManager;
        [Inject] private IBallGenerator _ballGenerator;
        [Inject] private GameStatusChangeSignal _gameChangeSignal;


        public void Initialize()
        {
            ShowStartMenu();
            _gameChangeSignal.Listen(ProceedChangesOfSession);
        }

        private void ProceedChangesOfSession(GameChangeType obj)
        {
            if (obj == GameChangeType.Ended)
            {
                ShowScore();
            }
        }

        private void ShowScore()
        {
            _ballGenerator.Stop();
            _uiManager.GetOrCreate<ScoreViewContorller>().Show();
        }

        public void ShowStartMenu()
        {
            _gameChangeSignal.Fire(GameChangeType.ExitToMainMenu);
            _uiManager.HideAllViews();
            _uiManager.GetOrCreate<MainMenuviewController>().Show();
        }

        public void StartGame()
        {
            _uiManager.HideAllViews();
            _uiManager.GetOrCreate<HudMenuViewController>().Show();
            _ballGenerator.Run();
            _gameSession.Start();
        }

        public int LastScore()
        {
            return _gameSession.CurrentScore;
        }
    }
}
