﻿using System;

namespace Assets.Scripts.GameCore
{
    public interface IGameSession
    {
        event Action OnChange;
        int CurrentScore { get; }
        int MissedScore { get; }
        int RemaininTime { get; }
        void Start();
        int CurrentTime { get; }
    }
}