﻿using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.GameCore
{

    public class GameSession: IGameSession,ITickable,IDisposable
    {
        [Inject] private GameSession.Settings _settings;
        [Inject] private GameStatusChangeSignal _gameChangeSignal;
        [Inject] private BallSignal _ballSignal;
        public event Action OnChange;

        private int _currentScore;
        public int CurrentScore { get { return _currentScore; }}
        private int _missed;
        public int MissedScore { get { return _missed; } }
        private float _remainingTime;

        public int RemaininTime
        {
            get { return (int)_remainingTime; } 
        }

        public int CurrentTime
        {
            get
            {
                return _settings.GameDuration - RemaininTime;
            }
        }

        public void IncrementScore(int value)
        {
            _currentScore+=value;
            InvokeOnChange();
        }

        private bool _started;
        public void Start()
        {
            Clear();
            _remainingTime = _settings.GameDuration;
            _started = true;
            _gameChangeSignal.Fire(GameChangeType.Started);
            _ballSignal.Listen(BallListener);
        }

        private void Clear()
        {
            _currentScore = 0;
            _missed = 0;
            _lastSecond = -1;
            _started = false;
        }


        public void IncrementMissed(int score)
        {
            _missed+=score;
            InvokeOnChange();
        }

        void InvokeOnChange()
        {
            if (OnChange != null)
            {
                OnChange.Invoke();
            }
        }

        private void BallListener(SpriteBall s, BallSignalType t) 
        {
            switch (t)
            {
                case BallSignalType.Clicked:
                    IncrementScore(s.CurrentPointsPerClick);
                    break;
                case BallSignalType.Missed:
                    IncrementMissed(s.CurrentPointsPerClick);
                    break;
                default: throw new NotImplementedException();
            }
        }

        private int _lastSecond = -1;
        public void Tick()
        {
            if (_started)
            {
                if (_lastSecond == -1)
                {
                    _lastSecond = (int)_remainingTime;
                }

                _remainingTime -= Time.deltaTime;

                if (_lastSecond != (int) _remainingTime)
                {
                    _lastSecond = (int) _remainingTime;
                    InvokeOnChange();
                }

                if (_remainingTime < 0)
                {
                    _ballSignal.Unlisten(BallListener);
                    _gameChangeSignal.Fire(GameChangeType.Ended);
                    _started = false;
                }
            }
        }
        [Serializable]
        public class Settings
        {
            public int GameDuration;
        }

        public void Dispose()
        {
            _ballSignal.Unlisten(BallListener);
            Clear();
        }
    }
}
