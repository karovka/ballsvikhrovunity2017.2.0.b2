﻿using Assets.Scripts.GameCore.Balls;
using System;
using System.Collections;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Assets.Scripts.GameCore
{
    public enum BallSignalType
    {
        Clicked,
        Missed
    }
    public class BallSignal:Signal<BallSignal, SpriteBall, BallSignalType>
    {
    }

    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteBall : MonoBehaviour
    {
        [Inject] private GameStatusChangeSignal _startEndSignal;
        [Inject] private BallSignal _ballSignal;
        [Inject] private SpriteBall.SpriteBallSetting _settings;
        [Inject] private IGameSession _gameSession;

        public int CurrentPointsPerClick
        {
            get;
            private set;
        }
        private SpriteRenderer _spriteRend;

        void Awake()
        {
            _spriteRend = GetComponent<SpriteRenderer>();
        }

        private void OnSessionChange(GameChangeType obj)
        {
            if (obj == GameChangeType.Started||obj==GameChangeType.ExitToMainMenu)
            {
                Despawn();
            }
        }

        private void Despawn()
        {
            if (gameObject.activeSelf)
            {
                _despawnAction.Invoke(this);
                _ballSignal.Fire(this, BallSignalType.Missed);
            }
        }

        private Action<SpriteBall> _despawnAction;

        public void InjectPool(BallsPool pool)
        {
            _despawnAction = pool.Despawn;
            _startEndSignal.Listen(OnSessionChange);
        }
        void OnEnable()
        {
            StartCoroutine(MoveUp());
        }
        public void OnMouseDown()
        {
            _ballSignal.Fire(this,BallSignalType.Clicked);
            _despawnAction.Invoke(this);
        }

        void OnDisable()
        {
            StopAllCoroutines();
        }
        private float _zPos;
        public void SetZ(float zPos)
        {
            _zPos = zPos;
        }
        public void SetPositionXY(Vector2 position)
        {
            transform.position  = new Vector3(position.x,position.y,_zPos);
            StartCoroutine(MoveUp());
        }
        private float _startSpeed;
        private IEnumerator MoveUp()
        {
            while (true)
            {
                yield return null;
                var speedCalculated = _startSpeed + _settings.AccelerationPerSec * _gameSession.CurrentTime;
                var speed = speedCalculated > _settings.MaxSpeed ? _settings.MaxSpeed : speedCalculated;
                gameObject.transform.Translate(0,speed*Time.deltaTime,0);
            }
        }
        void OnBecameInvisible()
        {
            Despawn();
        }

        private void Randomize()
        {
            var scaleFactor = Random.Range(0, 1f);
            var scale = (_settings.MinSize) + (_settings.MaxSize - _settings.MinSize) * scaleFactor;
            transform.localScale = Vector3.one * scale;
            _startSpeed = _settings.StartSpeedMinSize + (_settings.StartSpeedMaxSize - _settings.StartSpeedMinSize) * scaleFactor;
            //calculating coins for this size
            CurrentPointsPerClick = _settings.CoinsForMinSize + 
                (int)((_settings.CoinsForMaxSize - _settings.CoinsForMinSize) * scaleFactor);
            _spriteRend.material.SetColor("_ColorTint", ColorRandomizer.GetRandomColor());
        }

        [Serializable]
        public class SpriteBallSetting
        {
            [Tooltip("minimal scale of ball")]
            public float MinSize;
            [Tooltip("maximum scale of ball")]
            public float MaxSize;
            [Tooltip("coins for min scale of ball")]
            public int CoinsForMinSize;
            [Tooltip("coins for max scale of ball")]
            public int CoinsForMaxSize;
            [Tooltip("acceleration of balls speed per game session second")]
            public float AccelerationPerSec;
            [Tooltip("start speed of min scale ball")]
            public float StartSpeedMinSize;
            [Tooltip("start speed of max scale ball")]
            public float StartSpeedMaxSize;
            [Tooltip("max speed of balls")]
            public float MaxSpeed;

        }
        public class BallsPool : MemoryPool<SpriteBall>
        {
            private float _zPosCorrection;
            protected override void OnCreated(SpriteBall item)
            {
                base.OnCreated(item);
                //inject pool for correct despawn
                item.InjectPool(this);
                //for correct layer view, inject z Position
                _zPosCorrection -= 0.00001f;
                item.SetZ(_zPos + _zPosCorrection);

            }

            protected override void OnSpawned(SpriteBall item)
            {
                base.OnSpawned(item);
                item.Randomize();
                //calculating xy in screenspace, using sprite radius
                var xy = new Vector2(
                    Random.Range(_minX + (item._spriteRend.bounds.size.x)/ 2,
                    _maxX - (item._spriteRend.bounds.size.x) / 2)
                    ,_y-item._spriteRend.bounds.size.y);
                item.gameObject.SetActive(true);
                item.SetPositionXY(xy);
            }
            protected override void OnDespawned(SpriteBall item)
            {
                base.OnDespawned(item);
                item.gameObject.SetActive(false);
            }

            private float _zPos;
            private float _minX;
            private float _maxX;
            private float _y;
            public BallsPool()
            {
                _zPos = Camera.allCameras[0].farClipPlane-1;
                var leftDownCorner = Camera.allCameras[0].ScreenToWorldPoint(new Vector3(0,0, _zPos));
                _minX = leftDownCorner.x;
                _y = leftDownCorner.y;
                _maxX = Camera.allCameras[0].ScreenToWorldPoint(new Vector3(Screen.width,0, _zPos)).x;
            }
        }

    }


}
