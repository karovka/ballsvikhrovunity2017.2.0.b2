﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.GameCore.Balls
{
    public class ColorRandomizer
    {

        private static Color[] _randomColors = new Color[]
        {
            Color.white,
            Color.yellow,
            Color.red,
            Color.magenta,
            Color.grey,
            Color.cyan,
            Color.blue
        };

        public static Color GetRandomColor()
        {
            return _randomColors[UnityEngine.Random.Range(0, _randomColors.Length - 1)];
        }
    }
}
