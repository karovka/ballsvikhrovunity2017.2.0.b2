﻿using System;

namespace Assets.Scripts.GameCore
{
    public interface IBallGenerator
    {
        void Run();
        void Stop();
    }
}
