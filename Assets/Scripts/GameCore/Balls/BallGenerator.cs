﻿using System;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.GameCore
{
    public class BallGenerator: MonoBehaviour, IBallGenerator, IInitializable
    {
        [Inject] private Settings _settings;
        [Inject] private DiContainer _container;

        public void Initialize()
        {
            _container.BindMemoryPool<SpriteBall,SpriteBall.BallsPool>().FromComponentInNewPrefab(_settings.Ball).UnderTransformGroup("BallsPool");
        }

        private IEnumerator CreateBalls()
        {
            while (true)
            {
                yield return new WaitForSeconds(_settings.BallsInterval);
                _container.Resolve<SpriteBall.BallsPool>().Spawn();
            }
        }

        [ContextMenu("StartBalls")]
        public void Run()
        {
            StartCoroutine(CreateBalls());
        }

        [ContextMenu("StopBalls")]
        public void Stop()
        {
            StopAllCoroutines();
        }

        [Serializable]
        public class Settings
        {
            public SpriteBall Ball;
            public float BallsInterval;
        }
    }


}
