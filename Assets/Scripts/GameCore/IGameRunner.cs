﻿
namespace Assets.Scripts.GameCore
{
    public interface IGameRunner
    {
        void ShowStartMenu();
        void StartGame();
        int LastScore();
    }
}
